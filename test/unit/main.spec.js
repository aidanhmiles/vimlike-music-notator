describe('constants and libraries', function(){
    var Constants,
        Theory,
        Mousetrap,
        _;

  beforeEach(module('vimlikeMusicNotator'));

  describe('external libraries', function(){

      beforeEach(inject(function(_lodash_, _Mousetrap_) {
          _ = _lodash_;
          Mousetrap = _Mousetrap_;
      })); 

      it("should have Mousetrap", function(){ 
          expect(Mousetrap).toBeDefined();
      });

      it("should have Mousetrap.record", function(){ 
          expect(Mousetrap.record).toBeDefined();
      });
      // it("should have Mousetrap.bind", function(){ 
          // expect(Mousetrap.record).toBeDefined();
      // });

      it("should have _", function(){ 
          expect(_).toBeDefined();
      });



  });
  describe('theory constants', function(){

      beforeEach(inject(function(_Constants_) {
          Constants = _Constants_;
          Theory = Constants.theory;
      })); 

      it("gets NAME_FROM_ID", function(){
        Theory.NAME_FROM_ID([0,1]).should.eql("C#");
        Theory.NAME_FROM_ID([0,0]).should.eql("C");
        Theory.NAME_FROM_ID([6,9]).should.eql("Bbb");
      });

      it("gets ID_FROM_NAME", function(){
        Theory.ID_FROM_NAME("C#").should.eql([0,1]); 
        Theory.ID_FROM_NAME("Bbb").should.eql([6,9]);
      });

      it("gets QUALITY_FROM_INTERVALS", function(){
        Theory.QUALITY_FROM_INTERVALS([[0, 0], [2, 4], [4, 7]]).should.eql("M");
        Theory.QUALITY_FROM_INTERVALS([[0, 0], [2, 3], [4, 7]]).should.eql("m");

            
      });

      it("gets INTERVALS_FROM_QUALITY", function(){
        Theory.INTERVALS_FROM_QUALITY("M").should.eql([[0, 0], [2, 4], [4, 7]]);
        Theory.INTERVALS_FROM_QUALITY("m").should.eql([[0, 0], [2, 3], [4, 7]]);
      }); 
    });
});
