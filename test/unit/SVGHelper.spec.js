describe("SVGHelper", function(){

    var Note, _, SVGHelper, Constants;

    beforeEach(function(){
        module("vimlikeMusicNotator");

        inject(function(_Note_, _lodash_, _SVGHelper_, _Constants_){
            Note = _Note_;
            _ = _lodash_;
            SVGHelper = _SVGHelper_;
            Constants = _Constants_;
        });
    });

    describe("noteOffsetY", function(){

        it("positions a note according to absolute note value", function(){

            var note = new Note("F#3"),
                // F# is 1st space, 3 half step increments away from B line.
                expectedOffset = (Constants.spacing.measureHeight / 2) + (3 * Constants.spacing.halfStepHeight); 

                expect(note.getDiatonicID()).toBe(3);
                expect(SVGHelper.stepsFromMiddle(note)).toBe(3);
                expect(SVGHelper.noteOffsetY(note)).toBe(expectedOffset);
        });

        it("takes clef into account", function(){
            // not implemented
            expect(true).toBe(false);
        });
    });
});
