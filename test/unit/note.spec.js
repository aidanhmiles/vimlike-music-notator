describe("Note", function(){

    var Note,
        _;
        // ctrl,
        // scope

    beforeEach(function(){
        module("vimlikeMusicNotator");

        inject(function(_Note_, _lodash_){
            Note = _Note_;
            _ = _lodash_;
        });

        inject(function($controller, _$rootScope_){
            // ctrl = $controller("MainCtrl", {$scope: this.scope});
            // scope = _$rootScope_; 
        });

    });

    it("is a thing", function(){
        Note.should.be.a.function;
    });

    it("is a constructor that makes new notes, with options", function(){
        window.Note = Note;

        spyOn(window, 'Note');

        var testNote = new window.Note({register: 2, diatonicID: 4, chromaticID: 5});

        expect(window.Note).toHaveBeenCalledWith({register: 2, diatonicID: 4, chromaticID: 5});
    });

    it("constructor is Note", function(){
        var testNote = new Note({register: 2, diatonicID: 4, chromaticID: 5});
        testNote.constructor.name.should.eql("Note");
    });

    describe("has accessors for its properties:", function(){ 
        var testNote;
        beforeEach(function(){
            testNote = new Note([10, 18]);
        });

        it("diatonic and chromatic ID", function(){
            expect(testNote.getDiatonicID()).toBe(3); 
            expect(testNote.getChromaticID()).toBe(6); 
        });

        it("idPair", function(){
            expect(_.isEqual(testNote.getIDPair(), [3,6])).toBe(true);
        });

        it("absolute value diatonic / chromatic ID pair", function(){
            expect(_.isEqual(testNote.getAbsoluteIDPair(), [10,18])).toBe(true);
        });
    }); 

    it("can be constructed from Scientific Pitch Notation", function(){
        var testNote = new Note("F#3"); 

        expect(testNote.getRegister()).toBe(3);
        expect(_.isEqual(testNote.getIDPair(), [3,6])).toBe(true);

        expect(testNote.getDiatonicID()).toBe(3);
        expect(testNote.getChromaticID()).toBe(6);
    });

    it("can be constructed from a [diatonicID, chromaticID] pair", function(){
        var testNote = new Note([3, 6]);

        expect(testNote.getDiatonicID()).toBe(3);
        expect(testNote.getChromaticID()).toBe(6);
        expect(testNote.getRegister()).toBe(0);
        expect(testNote.getPitch()).toBe("F#");
        expect(_.isEqual(testNote.getIDPair(), [3,6])).toBe(true);
    });

    it("can be constructed from a [diatonicID, chromaticID] pair in multiple octaves", function(){
        var testNote = new Note([10, 18]);
        expect(testNote.getDiatonicID()).toBe(3);
        expect(testNote.getRegister()).toBe(1);
        expect(testNote.getPitch()).toBe("F#");
    });

    it("can be transposed", function(){
        var testNote = new Note([11, 19]);
        expect(testNote.getPitch()).toBe("G");

        var register = testNote.getRegister();
        expect(register).toBe(1);

        // transpose an octave
        testNote.transpose([7, 12]); 
        expect(testNote.getPitch()).toBe("G");
        expect(testNote.getRegister()).toBe(register + 1);
    });

    it("can be transposed negatively", function(){

        var testNote = new Note([4, 7]);
        expect(testNote.getPitch()).toBe("G");
        expect(testNote.getRegister()).toBe(0);

        testNote.transpose([-5, -9]); // perfect 6th
        expect(testNote.getPitch()).toBe("Bb");
        expect(_.isEqual(testNote.getIDPair(), [6,10])).toBe(true);
        expect(testNote.getRegister()).toBe(-1);
    });

    // it("can be equal to another note", function(){});
    // it("can be greater than another note", function(){});
    // it("can be less than another note", function(){});
    // it("knows if it is in a particular key", function(){});

});
