// // The basic unit of sheet music is a measure, which is broken usually into two segments. Notes may not cross over the divide between those segments (in 4/4 it's between the end of beat 2 and the beginning of beat 3, in 7/4 it's either between beat 4 and 5, or beat 3 and 4, etc). Those segments are further divided into groupings of notes and/or rests, according to the meter.

// song: {
//     title: "Whatever",
//     key: "Em",
//     meter: [4,4],

//     measures: [{
// 		key: false, // no key change
// 		meter: false, // no meter change
// 		ties: [
// 		{
// 			start: 0,
// 			end: 24
// 		}],
// 		notes: [{
// 			name: 'C4', // relative to song.key
// 			absValue: 0,
// 			relValue: ?, // scale degree of current chord / mode / or something? not sure if need this 
// 			duration: 16, // == (% of a measure) * (smallest unit), e.g. in 4/4, quarter note is .25 of a measure * let's say 64th is smallest division = 16 units
// 			position: 0, // unit position of the beginning of a note 
// 			articulation: ['staccato', 'martellato'], // other vals: 'tenuto', 'stacatissimo', 'marcato', or articulation: false 
// 		}, 
// 		{
// 			// empty note
// 		}] // end notes
// 	}] // end measures
// } // end song
