var paths = require('../paths');

module.exports = function(config) {

	config.set({
		files: paths.karmaFiles,

		// singleRun: false,
		autoWatch : true, 
		frameworks: ['jasmine'], 
		reporters: ['spec'],
		colors: true, 
		browsers : ['PhantomJS'], 
		plugins : [
			'karma-phantomjs-launcher',
			'karma-chrome-launcher',
			'karma-spec-reporter',
			'karma-jasmine'
		]
	});

};
