# WHAT IS THIS

It will be a simple version of a notation software like Sibelius, with fully customizeable keybindings (inspired by Vim). It will be open source the whole way through, and the intention is to learn how to do a bunch of cool things.

## Other

### Data Structure

#### Description
A song is made of measures, which are composed of two segments each. Segments are either a half, or close to a half, of a measure – in 4/4, those segments are 2 beats each, in 7/4, they would be 4 and 3 beats, interchangeably. Notes compose segments, and between notes are rests (nulls).

#### Example:

    song: {
	title: "Whatever",
	key: ['F#', 'C#', 'G#'],
	tempo: 96,

	pickup: null,

	measures: [{

	    keyChange: false,
	    tempoChange: false,

	    notes: [{
		idPair: [0,0],
		spnName: "C",
		duration: 16, // base modifier (64, for 64th note) * duration modifier (.25 for quarter note) = 16
		// the above lets me have a flexible smallest note value
		durationModifier: .25,
	    },{
		// another note here
	    }] 

	},{
	    // another measure here
	}]
    }
