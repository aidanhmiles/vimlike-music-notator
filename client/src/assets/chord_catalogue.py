from musictheorist.chord import Chord
from musictheorist.note import Note

from itertools import product


# A chord can be defined by a root (e.g. C4), a quality (e.g "M" for "major"),
# and an inversion
roots = [Note("C", 4), Note("G", 4), Note("F", 4)]
qualities = ["M", "m"]
inversions = [0, 1, 2]

# Generate the inversions of all major and minor chords in the keys of C, F,
# and G
all_chords = [
    Chord.from_quality(root, quality, inversion) for root, quality, inversion in
    product(roots, qualities, inversions)
]
for chord in all_chords:
    print(chord)
