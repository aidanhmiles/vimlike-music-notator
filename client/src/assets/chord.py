from blist import sortedset

from collections import deque

from .note import (Note, NUMBER_OF_NOTES_IN_DIATONIC_SCALE,
                   NUMBER_OF_NOTES_IN_CHROMATIC_SCALE)

QUALITY_TO_INTERVAL_LIST = {
    "M": ((0, 0), (2, 4), (4, 7)),
    "m": ((0, 0), (2, 3), (4, 7)),
    "7": ((0, 0), (2, 4), (4, 7), (6, 10))
}

INTERVAL_LIST_TO_QUALITY = {
    value: key for key, value in QUALITY_TO_INTERVAL_LIST.items()
}

def invert_interval_list(interval_list, inversion):
    """
    Moves around the intervals in an interval list to resemble an inverted
    chord.

    * (0, 0), (2, 4) == (root, 3rd)
    * rotated once will give you (2, 4), (7, 12) == (3rd, root an octave up)
    * rotated twice will give you (7, 12), (9, 16) == (root an octave up,
      3rd an octave up)

    A negitive value for inversion goes the other way.

    """
    interval_deque = deque(interval_list)
    if inversion >= 0:
        for _ in range(inversion):
            old_note_id = interval_deque.popleft()
            new_note_id = (old_note_id[0] + NUMBER_OF_NOTES_IN_DIATONIC_SCALE,
                           old_note_id[1] + NUMBER_OF_NOTES_IN_CHROMATIC_SCALE)
            interval_deque.append(new_note_id)

    else:
        for _ in range(abs(inversion)):
            old_note_id = interval_deque.pop()
            new_note_id = (old_note_id[0] - NUMBER_OF_NOTES_IN_DIATONIC_SCALE,
                           old_note_id[1] - NUMBER_OF_NOTES_IN_CHROMATIC_SCALE)
            interval_deque.appendleft(new_note_id)
    return tuple(interval_deque)


class Chord(sortedset):
    """
    A chord is a list of notes.

    The ``sortedset`` superclass ensures that the list will always be in
    ascending order, and that there will be no duplicates.

    """
    @classmethod
    def from_quality(cls, root, quality, inversion=0, duration=4):
        """Build a list of notes from a quality and a root."""
        initial_diatonic_id, initial_chromatic_id = \
                root.get_diatonic_id(), root.get_chromatic_id()
        interval_list = invert_interval_list(QUALITY_TO_INTERVAL_LIST[quality],
                                             inversion)
        note_list = [Note.from_ids(initial_diatonic_id + diatonic_interval,
                                   initial_chromatic_id + chromatic_interval,
                                   duration=duration)
                     for diatonic_interval, chromatic_interval in interval_list]
        return cls(note_list)

    def get_identity(self):
        """Returns a (root, quality, inversion) tuple."""
        interval_list = [(note.get_diatonic_id(), note.get_chromatic_id())
                         for note in self]
        for inversion in range(len(self)):
            # For each inversion, normalize the note ids so that lowest note
            # is (0, 0), and then sort it so that it matches with a quality
            # in INTERVAL_LIST_TO_QUALITY.
            base_diatonic_id, base_chromatic_id = min(interval_list)
            interval_list = [
                ((diatonic_id - base_diatonic_id) \
                        % NUMBER_OF_NOTES_IN_DIATONIC_SCALE,
                 (chromatic_id - base_chromatic_id) \
                        % NUMBER_OF_NOTES_IN_CHROMATIC_SCALE)
                for diatonic_id, chromatic_id in interval_list
            ]
            interval_list.sort()

            # If this quality matches, return it; otherwise, do another
            # inversion.  Inversions go **backwards**, because we're trying
            # to reverse-engineer our way back to root position.
            quality = INTERVAL_LIST_TO_QUALITY.get(tuple(interval_list))
            if quality is not None:
                break
            else:
                interval_list = invert_interval_list(interval_list, -1)
        else:
            return (None, "unknown", 0)

        # We call the "root" of the chord the first tonic note found beneath
        # the lowest note in the chord.
        bass_note_diatonic_id = self[0].get_diatonic_id()
        bass_note_chromatic_id = self[0].get_chromatic_id()
        diatonic_distance, chromatic_distance = \
                QUALITY_TO_INTERVAL_LIST[quality][inversion]
        root = Note.from_ids(bass_note_diatonic_id - diatonic_distance,
                             bass_note_chromatic_id - chromatic_distance)
        return (root, quality, inversion)

    def __str__(self):
        """There must be a better way to do this, no?"""
        string = "["
        string += ", ".join(str(element) for element in self)
        string += "]"
        return string
