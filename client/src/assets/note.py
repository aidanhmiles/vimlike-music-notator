from functools import total_ordering


NUMBER_OF_NOTES_IN_DIATONIC_SCALE = 7
NUMBER_OF_NOTES_IN_CHROMATIC_SCALE = 12


# Maps a note name to its associated (diatonic, chromatic) ID pair.
NAME_TO_ID = {
    "Cb":  (0, -1),
    "C":   (0, 0),
    "C#":  (0, 1),
    "C##": (0, 2),
    "Db":  (1, 1),
    "D":   (1, 2),
    "D#":  (1, 3),
    "Ebb": (2, 2),
    "Eb":  (2, 3),
    "E":   (2, 4),
    "E#":  (2, 5),
    "Fb":  (3, 4),
    "F":   (3, 5),
    "F#":  (3, 6),
    "F##": (3, 7),
    "Gb":  (4, 6),
    "G":   (4, 7),
    "G#":  (4, 8),
    "Ab":  (5, 8),
    "A":   (5, 9),
    "A#":  (5, 10),
    "Bbb": (6, 9),
    "Bb":  (6, 10),
    "B":   (6, 11),
    "B#":  (6, 12)
}


# Maps a (diatonic, chromatic) ID pair to its associated note name.
ID_TO_NAME = {value: key for key, value in NAME_TO_ID.items()}


@total_ordering
class Note(object):
    """
    A note is a pitch and a duration.

    The "register" argument follows Scientific Pitch Notation, detailed here:
    ``http://en.wikipedia.org/wiki/Scientific_pitch_notation``.

    A "duration" is an integer representing the number of 16th notes that
    compose the note.  As a reference:

    * 1 == sixteenth note
    * 2 == eighth note
    * 3 == dotted eighth note
    * 4 == quarter note
    * 6 == dotted quarter note
    * 8 == half note
    * 12 == dotted half note
    * 16 == whole note

    """
    def __init__(self, pitch, register, duration=4):
        self.pitch = pitch
        self.register = register
        self.duration = duration

    @classmethod
    def from_ids(cls, diatonic_id, chromatic_id, duration=4):
        """Creates a pitch/register pair from a diatonic and chromatic id."""
        register, diatonic_id = divmod(diatonic_id,
                                       NUMBER_OF_NOTES_IN_DIATONIC_SCALE)
        register, chromatic_id = divmod(chromatic_id,
                                        NUMBER_OF_NOTES_IN_CHROMATIC_SCALE)

        # These tests compensate for an unfortunate aspect of Scientific Pitch
        # Notation: that Cb4 == B3, and B#3 == C4.  This means that we cannot
        # keep the tuples in ID_TO_NAME to 0 <= n <= 11 if we're to have simple
        # get_*_id functions as below.
        if (diatonic_id, chromatic_id) == (0, 11):
            # case of NAME_TO_ID["Cb"]
            chromatic_id = -1
            register += 1
        elif (diatonic_id, chromatic_id) == (6, 0):
            # case of NAME_TO_ID["B#"]
            chromatic_id = 12
            register -= 1

        pitch = ID_TO_NAME[(diatonic_id, chromatic_id)]
        return cls(pitch, register, duration)

    def get_diatonic_id(self):
        return self.register * NUMBER_OF_NOTES_IN_DIATONIC_SCALE + \
                NAME_TO_ID[self.pitch][0]

    def get_chromatic_id(self):
        return self.register * NUMBER_OF_NOTES_IN_CHROMATIC_SCALE + \
                NAME_TO_ID[self.pitch][1]

    def __str__(self):
        return self.pitch + str(self.register)

    def __eq__(self, other):
        """Equality does **not** consider durations; just relative pitches."""
        return self.get_chromatic_id() == other.get_chromatic_id() and \
                self.get_diatonic_id() == other.get_diatonic_id()

    def __lt__(self, other):
        """Chromaticism takes precedence over diatonicism."""
        if self.get_chromatic_id() < other.get_chromatic_id():
            return True
        if self.get_chromatic_id() == other.get_chromatic_id() and \
                self.get_diatonic_id() < other.get_diatonic_id():
            return True
        return False
