// // includes model (factory) and directive

// angular.module('vimlikeMusicNotator')
// .factory('Note', ['Constants', 'lodash', 'SVGHelper', function(Constants, _, SVGHelper) {
//     return function Note(options, _duration) { // constructor


//         var _pitch,
//             _register = 0,
//             _duration,
//             _pitch,
//             _diatonicID,
//             _chromaticID,
//             self = this, // trying it out to see how i like it
//             _startPostion; // is this where this goes? TODO

//         // ----- CONSTRUCTORS -----

//         // new Note([0, 2], duration)
//         if (_.isArray(options)) {
//             buildFromIDPair(options);
//             _duration = _duration || .25;
//         }

//         // new Note('C#3', duration)
//         else if (_.isString(options)) {
//             buildFromSPNName(options);
//             _duration = _duration || .25;
//         }

//         // new Note({ register: 3, diatonicID: 4, etc : etc })
//         else if (_.isPlainObject(options)) {

//             _duration = options._duration || .25;

//             if (options.spnName) {
//                 // 'C#3'
//                 // thus, the constructor prioritizes SPN name over the ID pair.
//                 // TODO how feel about this?
//                 buildFromSPNName(options.spnName)
//             }

//             else if (options.IDPair) {
//                 buildFromIDPair(options.IDPair);
//             }

//         } else { // if not one of those constructors
//             throw 'WTF??'
//         }

//         // END constructor conditionals

//         // builder / helper functions
//         function buildFromIDPair(pair) {

//             _diatonicID = pair[0],
//             _chromaticID = pair[1];

//             normalizeIDPair();

//             _pitch = Constants.theory.NAME_FROM_ID([_diatonicID, _chromaticID]);

//             checkForCFlatOrBSharp();
//         }

//         function normalizeIDPair() {

//             while (_diatonicID < 0) {
//                 _diatonicID += Constants.theory.DIATONIC_SCALE_LENGTH;
//                 _chromaticID += Constants.theory.CHROMATIC_SCALE_LENGTH;
//                 _register -= 1;
//             }

//             _register += ~~(_diatonicID / Constants.theory.DIATONIC_SCALE_LENGTH);
//             //
//             // whenever the idPair changes, normalizeIDPair will be called
//             // this includes building and transposing notes
//             // thus, by defaulting register to 0 and adding to it whenever the idPair
//             // changes, it stays up to date.

//             // if register from diatonicID is not the same as from chromaticID, this is a serious WTF
//             if (!_register === ~~(_chromaticID / Constants.theory.CHROMATIC_SCALE_LENGTH)) {
//                 throw 'WTF???';
//             }

//             _diatonicID = ~~(_diatonicID % Constants.theory.DIATONIC_SCALE_LENGTH);
//             _chromaticID = ~~(_chromaticID % Constants.theory.CHROMATIC_SCALE_LENGTH);
//         }

//         function buildFromSPNName(spnString) {
//             // spnString is something like 'C#3'

//             _register = parseInt(spnString.slice(-1));

//             if (_.isNaN(_register)) {
//                 throw 'NO REGISTER PROVIDED, try again'
//             } // or should it default to 3 or 4 or something?

//             _pitch = spnString.slice(0, -1);

//             var idPair = Constants.theory.ID_FROM_NAME(_pitch);

//             _diatonicID = idPair[0];
//             _chromaticID = idPair[1];

//             checkForCFlatOrBSharp();
//         }

//         function checkForCFlatOrBSharp() {
//             // These tests compensate for an unfortunate aspect of Scientific Pitch
//             // Notation: that Cb4 == B3, and B#3 == C4.  this means that we cannot
//             // keep the tuples in ID_TO_NAME to 0 <= n <= 11 if we're to have simple
//             // get_*_id functions as below.

//             if (_.isEqual([_diatonicID, _chromaticID], [0, 11])) {
//                 // case of NAME_TO_ID['Cb']
//                 _chromaticID = -1;
//                 _register += 1;

//             } else if (_.isEqual([_diatonicID, _chromaticID], [6, 0])) {
//                 // case of NAME_TO_ID['B#']
//                 _chromaticID = 12;
//                 _register -= 1;
//             }
//         }

//         // END builder / helper functions

//         // ----- PUBLIC METHODS -----
//         //
//         //

//         // getters and setters
//         //
//         self.getDiatonicID = function() {
//             return _diatonicID;
//         };
//         self.getChromaticID = function() {
//             return _chromaticID;
//         };

//         self.getIDPair = function() {
//             return [_diatonicID, _chromaticID];
//         };
//         self.getAbsoluteIDPair = function() {
//             return [
//                 _diatonicID + (Constants.theory.DIATONIC_SCALE_LENGTH * _register),
//                 _chromaticID + (Constants.theory.CHROMATIC_SCALE_LENGTH * _register)
//             ];
//         }

//         self.setDuration = function(newDuration) {
//             _duration = newDuration;
//         };
//         self.getDuration = function() {
//             return _duration;
//         };

//         self.setRegister = function(newRegister) {
//             _register = newRegister
//         };
//         self.getRegister = function() {
//             return _register;
//         };

//         // TODO
//         self.getPitch = function() {
//             return _pitch;
//         };
//         self.getSPNName = function() {
//             return _pitch + _register;
//         };


//         // other stuff
//         //
//         self.toString = function() {};
//         // return self.pitch + str(self.register)

//         // COMPARATORS
//         self.eqls = function(otherNote) {
//             return !!otherNote.getIDPair && _.isEqual(self.getIDPair(), otherNote.getIDPair());
//         };

//         self.lessThan = function(otherNote) {
//             if (self.getChromaticID < otherNote.getChromaticID) {
//                 return true;
//             }
//             else if (self.getChromaticID == otherNote.getChromaticID &&
//                     self.getDiatonicID < otherNote.getDiatonicID) {
//                 return true;
//             }
//             return false;
//         };

//         self.greaterThan = function(otherNote) {
//             if (self.getChromaticID > otherNote.getChromaticID) {
//                 return true;
//             }
//             else if (self.getChromaticID == otherNote.getChromaticID &&
//                     self.getDiatonicID > otherNote.getDiatonicID) {
//                 return true;
//             }
//             return false;
//         };

//         self.transpose = function(idPair) {

//             _diatonicID += idPair[0];
//             _chromaticID += idPair[1];


//             normalizeIDPair();

//             _pitch = Constants.theory.NAME_FROM_ID([_diatonicID, _chromaticID]);

//             checkForCFlatOrBSharp();

//             // fix display
//             self.posYBus.push(SVGHelper.noteOffsetY(self));
//             self.posXBus.push(SVGHelper.noteOffsetX(self));
//         };

//         // transposes a note # of steps through a scale
//         self.transposeInScale = function(mode, transAmt) {};

//         // reports whether a note is 'in' a given key or mode
//         // helps with accidentals
//         self.isInKey = function(keySig) {};

//         self.clone = function() {};
//     }; // end constructor

// }])

// .directive('note', ['SVGHelper', 'Mousetrap', 'Bacon', function(SVGHelper, Mousetrap, Bacon) {
//     return {
//         restrict: 'E',
//         templateUrl: 'note.html',
//         templateNamespace: 'svg',
//         controllerAs: 'noteCtrl',
//         bindToController: true,
//         replace: true,
//         scope: {
//             note: '=',
//             measure: '='
//         },

//         // link: function(scope, elem, attrs, controller){
//         // },
//         controller: function($scope, $element) {
//             var self = this,
//                 note = this.note;

//             note.measure = this.measure; // link note to its measure because want note to be context-aware

//             // -- init
//             note.posX = 0;
//             note.posY = 0;

//             // -- end init

//             // events!
//             // since this is display logic, i'm keeping it in the directive rather than factory
//             note.posYBus = new Bacon.Bus();
//             note.posYBus.onValue(function(val) {
//                 // $scope.$apply(function(){
//                     note.posY = val;
//                 // });
//             });
//             note.posYBus.push(SVGHelper.noteOffsetY(note));


//             note.posXBus = new Bacon.Bus();
//             note.posXBus.onValue(function(val) {
//                 // $scope.$apply(function(){
//                     note.posX = val;
//                 // });
//             });
//             note.posXBus.push(SVGHelper.noteOffsetX(note));


//             //
//             // event handlers
//             //
//             var clicks = Bacon.fromEvent($element, 'click').onValue(function() {
//                 var idx = SVGHelper.activeNotes.indexOf(note);

//                 if (idx === -1) {
//                     SVGHelper.activeNotes.push(note);
//                     $scope.$apply(function() {
//                         self.isSelected = true;
//                     });
//                 } else {
//                     SVGHelper.activeNotes.splice(idx, 1);
//                     $scope.$apply(function() {
//                         self.isSelected = false;
//                     });
//                 }
//                 console.log('--- Active Notes ---');
//                 console.log(SVGHelper.activeNotes);

//             });
//         }
//     };
// }])

// .directive('stem', [function() {
//     return {
//         restrict: 'E',
//         templateUrl: 'stem.html',
//         bindToController: true,
//         controllerAs: 'stemCtrl',
//         templateNamespace: 'svg',
//         replace: true,
//         scope: {
//             note: '='
//         }

//         // controller: function($scope, $element, $attrs){

//         // },

//         // link: function(scope, elem, attrs, controller){
//         //  var note = controller.note;

//         //  if (SVGHelper.stepsFromMiddle(note) > 0){
//         //      controller.top = note.posY - 25;
//         //      controller.bottom = note.posY;
//         //      controller.posX = note.posX + 5;

//         //  } else {
//         //      controller.top = note.posY;
//         //      controller.bottom = note.posY + 25;
//         //      controller.posX = note.posX - 5;
//         //  }
//         // },
//     };
// }]);
