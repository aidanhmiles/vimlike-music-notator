
import { Component } from '@angular/core';

@Component({
  selector: 'song',
  template: require('../../templates/main.html')
})

export class SongComponent {
  measures = [1,2,3,4,5];
}

