angular.module('vimlikeMusicNotator')
.directive('timesig', function() {
	return {
		restrict: 'E',
		templateUrl: 'timesig.html'
	};
});
