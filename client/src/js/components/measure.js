
// TODO:
// - figure out auto-parsing empty space as rests (sketch at bottom)
// - trigger parsing on new note / new rest (is that the same as delete note?)
// - nextMeasure
// - make position or index for notes, rests, and measures, within respective parents
// - is it measure, or note/rest, which keeps track of startPos? (sketch at bottom)
// - startPos + duration = enough info to compare notes over time
// - - note.overlapsWith(otherNote)
// - - note.overwrites(otherNote)
// - - note.adjustTo(otherNote)
// - - - takes notes that exist, and chops them up to accommodate a new note
// - - - e.g. insert note at position, and until .overlapsWith returns false, modify existing notes or rests
// - - - make pop up for more fine-tuned insertion, or make a view with expanded rests, for precise note-dropping?

import { Component, Input } from '@angular/core';
const Snap = require('../../../vendor/snap.svg.js').Snap;

@Component({
  selector: 'measure',
  template: require('../../templates/measure.html')
})
export class MeasureComponent {

  @Input() numberThing;

    // private svg: any; // the SVG canvas for this measure object
  constructor() {
    console.log(this.numberThing);
  }

  ngAfterViewInit() {
    this.draw();
  }

  draw() {
    // if (this.svg){ // if the SVG canvas
    //     // clear the canvas
    // }
    let id = `#measure${this.numberThing}`;
    let svg = Snap(id);
    let circle = svg.circle(50,40,10);

    circle.attr('fill', '#f00');
  }

}

// angular.module('vimlikeMusicNotator')
// .factory('Measure', ['Constants', 'Bacon', 'lodash', function(Constants, Bacon, _) {
//     return function Measure(options) {
//         var _notes = [],
//             _rests = [],
//             // _contents = [[],[]], // every measure contains 2 segments
//             _contents = [],
//             _position,
//             _timeSig,
//             _keySig,
//             // maintain a reference to the next measure, for when note insertion in this
//             // measure has a remainder that must be inserted in the next measure
//             _nextMeasure = null,

//             // event buses
//             _noteBus = new Bacon.Bus(), // push new notes into this
//             _notesStream,
//             _contentsStream,
//             _restsStream,
//             _restBus = new Bacon.Bus(), // push new rests into this
//             _contentsBus = new Bacon.Bus(); // automatically populates with noteBus and restBus

//             // end var

//             // connect the buses
//             _contentsBus.plug(_restBus);
//             _contentsBus.plug(_noteBus);

//         // when noteBus is updated, process the new note and return
//         // the new note list
//         _notesStream = _noteBus.map(function(newNote) {


//             // 1) go through _contents to find what occupies
//             // desired position of the new item
//             //
//             // 2) truncate previous item at new item's start position,
//             //    plus modify (or delete) any item(s) with which new
//             //    item overlaps
//             // 3)

//             // push new note into notes
//             _notes.push(newNote);

//             return _notes;
//         }).log('Notes:');


//         // returns the sorted contents of the measure
//         _contentsStream = _contentsBus.onValue(function(newItem) {

//             _contents = _(_contents).sortBy(function(item) {
//                 return item.startPos();
//             });

//             return _contents;
//         });

//         // public methods
//         this.insertNote = function(note, position) {
//             // dummy implementation
//             _noteBus.push(note);

//             // 'the real thing' in pseudocode
//             //
//             //
//         }

//         // bacon template will update when any prop / stream updates
//         this.baconTemplate = Bacon.combineTemplate({
//             // streams
//             notesStream: _notesStream.toProperty([]), // toProperty necessary?
//             notes: _notes,
//             notesStream: _notesStream,
//             noteBus: _noteBus,
//             rests: _restsStream,
//             contentsStream: _contentsStream,
//             contents: _contents,

//             // properties
//             position: _position

//         }).log('Measure: ');
//     };

// }])

// .directive('measure', ['Constants', function(Constants) {
//     return {
//         restrict: 'E',
//         templateUrl: 'measure.html',
//         scope: {
//             measure: '='
//         },
//         bindToController: true,
//         controllerAs: 'measureCtrl',
//         controller: function($scope, $element, $attrs) {

//             console.log(this.measure);

//             var that = this;

//             this.measure.baconTemplate.onValue(function(val) {

//                 that.baconTemplate = val;

//             });

//         } // end controller
//     };
// }]);

// ----- SKETCH TIME -----
// This is how measures will be parsed / broken down
//
// new Measure() // with no args
// // defaults to timeSig, keySig of self.song
// => [[new Rest(4), new Rest(4)],[new Rest(4), new Rest(4)]]
// OR
// => [new Rest(16)]?
//
// graphic illustration
// // one hyphen is one base unit (let's assume a 16th)
// // let {} represent groupings
//
// Case: empty measure
//
// 1) [{----------------}]
//
// 2) [new Rest(16)]
//
// 3) rest class determines how to display it
//
// Case: measure with some notes
//
// 1) [[new Note('A3', 4), {----}, new Note('A3', 2), {------}], [{--}, new Note('A3', 4), {------}, new Note('A3', 6)]]
// - measure is split into two segments
// 2) [[new Note('A3', 4), new Rest('A3', 4), new Note('A3', 2), new Rest(6)], [new Rest(2), new Note('A3', 4), new Rest(6), new Note('A3', 6)]]


// _contents = [ {
//     startPos: 0,
//     item: new Note("F#3", 4),
//     duration: function(){ return this.item.getDuration(); }
// },{
    // again
// }]



