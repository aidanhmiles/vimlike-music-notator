angular.module('vimlikeMusicNotator')
.controller('MainCtrl', [
    '$scope', '$interval', '$document', 'Mousetrap', 'SVGHelper',
    'Note', 'Bacon', 'Song', 'Measure', 'vimlInfoService',
    function($scope, $interval, $document, Mousetrap, SVGHelper,
            Note, Bacon, Song, Measure, vimlInfoService) {

    var self = this;

    // when Song updates, update $scope property
    Song.baconTemplate.digest($scope, 'song');

    var allKeyDowns = Bacon.fromEvent($document, 'keydown');

    // spacebar 32
    // f 70
    // j 74
    function isKeyCode(keyCode) {
        return function(event) {
            return event.keyCode === keyCode;
        };
    }


    // key press
    //  - check if is first key in combo
    //  - start timeout
    //  - if timeout expires, do action for that key
    //  - if we get another key, check if it's in a combo with first key
    //      - if it is, do that combo's action
    //      - if not, do each individual action?
    //  -

    // var allKeyUps = Bacon.fromEvent($document, 'keyup').onValue(function(val){
    //     console.log('up', val);
    // });

    var spaceBar = allKeyDowns.filter(isKeyCode(32));
    var fKey = allKeyDowns.filter(isKeyCode(70));

    var state = Bacon.combineTemplate({
        space: spaceBar,
        fKey: fKey
    });

    state.log();


    // most recent keydown history
    allKeyDowns.scan([], function(prev, next) {
        if (prev.length > 3) {
            prev.shift();
        }
        prev.push(next);
        return prev;

    }).onValue(function(val) {
        vimlInfoService.info.push(val);
    });

    // - - - - - Mousetrap bindings - - - - -

    Mousetrap.bind('i', function() {
        Song.measuresBus.push(new Measure());
    });

    Mousetrap.bind('h', function() {
        SVGHelper.activeNotes.forEach(function(note) {
            note.posXBus.push(-5);
        });
    });

    Mousetrap.bind('l', function() {
        SVGHelper.activeNotes.forEach(function(note) {
            note.posXBus.push(5);
        });
    });

    Mousetrap.bind('j', function() {
        SVGHelper.activeNotes.forEach(function(note) {
            $scope.$apply(function() {
                note.transpose([-1, -2]);
            });
        });
    });

    Mousetrap.bind('k', function() {
        SVGHelper.activeNotes.forEach(function(note) {
            $scope.$apply(function() {
                note.transpose([1, 2]);
            });
        });
    });

}]);
