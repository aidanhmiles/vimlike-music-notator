import { RouterModule } from '@angular/router';

import { WelcomeComponent } from './components/welcome.component';
import { OtherComponent } from './components/other.component';

const appRoutes = [
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'other',
    component: OtherComponent
  },
  {
    path: '**',
    component: OtherComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);

export const appRoutingProviders = [];

