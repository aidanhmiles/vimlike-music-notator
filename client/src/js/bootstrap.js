import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

document.addEventListener('DOMContentLoaded', main);

/*
 * Bootstrap our Angular app with a top level NgModule
 */
import { AppModule } from './app.module';

function main() {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch((err) => console.log(err));
}

