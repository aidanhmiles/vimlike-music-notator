
// core modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

require('../../src/scss/index.scss');

// project modules
import { WelcomeComponent } from './components/welcome.component';
import { OtherComponent } from './components/other.component';
import { SongComponent } from './components/song.component';
import { MeasureComponent } from './components/measure';
import { AppComponent } from './app.component';
import { routing, appRoutingProviders } from './app.routes';

// app module definition
@NgModule({
  imports: [
    BrowserModule,
    routing
  ],
  declarations: [
    WelcomeComponent,
    OtherComponent,
    AppComponent,
    MeasureComponent,
    SongComponent
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
