// angular.module('vimlikeMusicNotator')
// .service('SVGHelper', ['Constants', function(Constants) {
//     //
//     // this service provides convenience functions for displaying SVG elements
//     // mainly to keep display logic and music theory entirely separate

//     return {
//         noteOffsetY: function(note) {
//             return (Constants.spacing.measureHeight / 2) + (this.stepsFromMiddle(note) * Constants.spacing.halfStepHeight);
//         },

//         noteOffsetX: function(note) {
//             console.log(note.measure);
//             var offset = note.measure.baconTemplate.notesStream.indexOf(note) * 20 + 25;
//             return offset;
//         },

//         stepsFromMiddle: function(note) {
//             // assuming treble clef for the time being (TODO)
//             // absolute id pair for middle B is [27, 47]
//             return [27, 47][0] - note.getAbsoluteIDPair()[0];
//         },

//         // for visual mode / moving and selecting notes
//         activeNotes: []

//     }; // end return statement
// }]);

import { Injectable } from '@angular/core';
const Snap = require('../../../vendor/snap.svg.js').Snap;

@Injectable()
export class SVGHelper {

  static insertStaff(){
    // insert each line
    // make group
    // return group
  }

  static insertTwoStaves(){
  }
}
