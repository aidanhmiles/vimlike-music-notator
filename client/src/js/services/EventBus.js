import { Injectable } from '@angular/core';


@Injectable()
export class EventBus {

}

// angular.module('vimlikeMusicNotator')
// .service('EventBus', ['$rootScope', function() {

// 	// public API
// 	var EventBus = {
// 		dispatch: {},
// 		addListener: function() {}
// 	};

// 	var registrations,
//         getListeners = function(type, useCapture) {
//             var captype = (useCapture ? '1' : '0') + type;

//             if (!(captype in registrations)) {
//                 registrations[captype] = [];
//             }

//             return registrations[captype];
//         },

//         init = function() {
//             registrations = {};
//         };

//     init();

// 	angular.extend(EventBus, {
//         addEventListener: function(type, listener, useCapture) {
//             var listeners = getListeners(type, useCapture);
//             var idx = listeners.indexOf(listener);
//             if (idx === -1) {
//                 listeners.push(listener);
//             }
//         },

// 		// e.g.
// 		// eventbus.addeventlistener('transpose', transposehandlerfunc, false)
//         removeEventListener: function(type, listener, useCapture) {
//             var listeners = getListeners(type, useCapture);
//             var idx = listeners.indexOf(listener);
//             if (idx !== -1) {
//                 listeners.splice(idx, 1);
//             }
//         },

// 		createEvent: function(type, extraData) {
// 			var evtData = {
// 				type: type,
// 				data: extraData
// 			};

// 			return evtData;
// 		},

//         dispatchEvent: function(evt) {
//             var listeners = getListeners(evt.type, false).slice();
//             for (var i = 0; i < listeners.length; i++) {
//                 listeners[i].call(this, evt);
//             }
//             return !evt.defaultPrevented;
//         }
// 	});

// 	return EventBus;
// 	// -----------------------------------
// 	// ideas
// 	// -----------------------------------
// 	//
// 	// EventBus.dispatch['note.transpose']();

// 	// 1)
// 	// Note.onclick = function(){
// 	//   var evt = new CustomEvent('note.activate', {
// 	//     type: 'activate',
// 	//     note: self,
// 	//     etc,
// 	//     etc
// 	//	 EventBus.dispatchEvent(evt);
// 	// };
// 	//
// 	//   EventBus.addEventListener('note.activate', function(e){ activeNotes.push(e.note);});
// 	//   TODO how does note object get sent as arg to above callback?
// 	// 2)
// 	// Mousetrap.bind('h', function(){
// 	//   if (MODE === 'normal') {}
// 	//   else if (MODE === 'visual') {
// 	//		activeNotes.each(function(note){
// 	//		  note.transpose(someVariable);
// 	//		})
// 	//   })
// 	//
// 	// var myEvent = new CustomEvent('note.transpose', {
// 	//	 type: 'transpose',
// 	//	 note: note,
// 	//	 bubbles: true,
// 	//	 cancelable: false
// 	// });

// 	// -----------------------------------
// 	// Dash.js EventBus
// 	// -----------------------------------
//     // var registrations,

//     //     getListeners = function(type, useCapture) {
//     //         var captype = (useCapture? '1' : '0') + type;

//     //         if (!(captype in registrations)) {
//     //             registrations[captype] = [];
//     //         }

//     //         return registrations[captype];
//     //     },

//     //     init = function () {
//     //         registrations = {};
//     //     };

//     // init();

// 	// return {
//         // addEventListener: function (type, listener, useCapture) {
//             // var listeners = getListeners(type, useCapture);
//             // var idx = listeners.indexOf(listener);
//             // if (idx === -1) {
//                 // listeners.push(listener);
//             // }
//         // },
// 		//
// 		// e.g.
// 		// EventBus.addEventListener('transpose', transposeHandlerFunc, false)
// 		//

//         // removeEventListener: function (type, listener, useCapture) {
//             // var listeners = getListeners(type, useCapture);
//             // var idx= listeners.indexOf(listener);
//             // if (idx !== -1) {
//                 // listeners.splice(idx, 1);
//             // }
//         // },

//         // dispatchEvent: function (evt) {
//             // var listeners = getListeners(evt.type, false).slice();
//             // for (var i= 0; i < listeners.length; i++) {
//                 // listeners[i].call(this, evt);
//             // }
//             // return !evt.defaultPrevented;
//         // }
// 	// };
// }]);
