import { Injectable } from '@angular/core'; 

import _ = require('lodash');

const NAME_TO_ID_PAIR = {
    'Cb':  [0, -1],
    'C':   [0, 0],
    'C#':  [0, 1],
    'C##': [0, 2],
    'Db':  [1, 1],
    'D':   [1, 2],
    'D#':  [1, 3],
    'Ebb': [2, 2],
    'Eb':  [2, 3],
    'E':   [2, 4],
    'E#':  [2, 5],
    'Fb':  [3, 4],
    'F':   [3, 5],
    'F#':  [3, 6],
    'F##': [3, 7],
    'Gb':  [4, 6],
    'G':   [4, 7],
    'G#':  [4, 8],
    'Ab':  [5, 8],
    'A':   [5, 9],
    'A#':  [5, 10],
    'Bbb': [6, 9],
    'Bb':  [6, 10],
    'B':   [6, 11],
    'B#':  [6, 12]
}; 

const QUALITY_TO_INTERVAL_LIST = {
    'M': [[0, 0], [2, 4], [4, 7]],
    'm': [[0, 0], [2, 3], [4, 7]],
    '7': [[0, 0], [2, 4], [4, 7], [6, 10]]
}; // to be expanded?

@Injectable()
export class AppConstants {

    // -- -- -- -- -- //
    // THEORY
    // -- -- -- -- -- //

    static NAME_TO_ID_PAIR = NAME_TO_ID_PAIR;
    static DIATONIC_SCALE_LENGTH: number = 7;
    static CHROMATIC_SCALE_LENGTH: number = 12;
    // sharps and flats as they appear in order in the Circle, starting from the key of C
    static CIRCLE_OF_FLATS: [string] = ['Bb', 'Eb', 'Ab', 'Db', 'Gb', 'Cb', 'Fb', 'Bbb'];
    static CIRCLE_OF_SHARPS: [string] = ['F#', 'C#', 'G#', 'D#', 'A#', 'E#', 'B#', 'F##'];

    ID_FROM_NAME(spnName: string) {
        return NAME_TO_ID_PAIR[spnName];
    }

    NAME_FROM_ID(idPair: [number, number]) {
        let wrapped = _(NAME_TO_ID_PAIR);

        return wrapped.findKey(function(value: [number]) {
            return _.isEqual(idPair, value);
        });
    }

    INTERVALS_FROM_QUALITY(quality: string) {
        return QUALITY_TO_INTERVAL_LIST[quality];
    }

    QUALITY_FROM_INTERVALS(intervals: [number]) {
        var wrapped = _(QUALITY_TO_INTERVAL_LIST);

        return wrapped.findKey(function(value: [number]) {
            return _.isEqual(intervals, value);
        });
    }

    // -- -- -- -- -- //
    // SPACING
    // -- -- -- -- -- //

    static verticalNoteCapacity: number = 28;
    static halfStepHeight: number = 5;
    static b3IdPair: [number] = [27, 47];
    static measureHeight: number = 140; 
    static q: number = 20; // distance between quarter notes

    static sixteenth: number = 0.42;
    static eighth: number = 0.65;
    static quarter: number = 1; // redundant, but need to have a property for each note type
    static whole: number = (3 * 0.65);

}
