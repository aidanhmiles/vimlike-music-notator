// angular.module('vimlikeMusicNotator')
// .service('Song', ['Constants', 'Bacon', function(Constants, Bacon) {

//     // - - - - - VARS - - - - -
//     //
//     // smallest unit of time in the song
//     // this will eventually be user-configurable
//     // multiply by base of timeSig, e.g. base unit here is 4 units per beat * 4 beats per measure = 16th note
//     var _baseUnitsPerBeat = 4,
//         // default timeSig for all classes
//         // can be overridden by individual measures
//         _timeSig = [4,4],
//         _key = ['F#', 'C#', 'G#', 'another', 'and another', 'anything goes here for now'],

//         _measures = [],

//         _measuresBus = new Bacon.Bus(),

//         _measuresStream = _measuresBus.map(function(newItem) {

//             // eventually, logic for inserting measures before the end
//             _measures.push(newItem);

//             return _measures;
//         });
//     // end var statement

//     // - - - - - FUNCS - - - - -

//     this.getMeasures = function() { return _measures; };

//     this.measuresBus = _measuresBus;

//     this.baconTemplate = Bacon.combineTemplate({
//         measuresStream: _measuresStream,
//         // measuresBus:
//         key: this.key

//     }).log('Song:  ');

//     return this;

//     // TODO implement .load, .save, other REST calls
// }]);

