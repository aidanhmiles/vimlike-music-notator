#!/bin/bash

# use my aliases
shopt -s expand_aliases

# requires tab command
# https://gist.github.com/bobthecow/757788
source $HOME/dotfiles/tab.bash

# requires renaming functions from bashp
# http://superuser.com/a/344397/302995
source $HOME/dotfiles/.bash_profile

# set me up
rename_window viml;

tab "rename_tab test && cd test/unit && vim -p *" 

#
# JS_FILES for editing; don't want all of them
#
declare -a JS_FILES=(\
measure.js \
note.js \
rest.js \
VME.controllers.js \
SVGHelper.js \
Constants.js \
) 
tab "rename_tab js && cd src/js && vim -p ${JS_FILES[@]}"

# templates
declare -a TPLS=(\
measure.html \
note.html \
rest.html \
stem.html \
main.html \
) 
tab "rename_tab templates && cd src/templates && vim -p ${TPLS[@]}"

tab "rename_tab scss && cd src/scss && vim -p *"

tab
 
rename_tab gulp && gulp;
